/*
 * Author: M4R70
 *
 * DiccNatAlpha.
 * 
 */

#ifndef DiccNatAlphaChimichanga
#define DiccNatAlphaChimichanga

#include <ostream>
#include "aed2/TiposBasicos.h"
#include "aed2.h"

namespace aed2
{

template <class T>
class DiccNatAlpha
{

	public:
	
	// crea dicc vacio
	DiccNatAlpha();
	
	//define un valor en el dicc
	
	void definir(const Nat,const T&);
	
	//indica si la clave esta definida o no en el dicc
	
	bool definido(const Nat) const;
	
	//obtiene el significado de una clave en particular
	
	const T& significado(const Nat) const;
	
	//borra la clave indicada del dicc
	
	void borrar(const Nat);
	
	//devuelve un iterador al conjunto de claves del diccionario
	
	Conj<Nat>::const_Iterador claves() const;

	private:
	
	
	Arreglo<T> array;
	Arreglo<bool> definidos;
	Arreglo<Conj<Nat>::Iterador> iteradores;
	Conj<Nat> cclaves;
	Nat largo;
	

};

template<class T>
DiccNatAlpha<T>::DiccNatAlpha(){
	array = Arreglo<T>(); 
	definidos = Arreglo<bool>();
	iteradores = Arreglo<Conj<Nat>::Iterador>();
	largo = 0;
}

template<class T>
void  DiccNatAlpha<T>::definir(const Nat i,const T& A){
	if( i >= largo) {
		Arreglo<T> ar = Arreglo<T>(i+1);
		Arreglo<bool> arDef = Arreglo<bool>(i+1);
		Arreglo<Conj<Nat>::Iterador > arIt = Arreglo<Conj<Nat>::Iterador >(i+1);
		for(Nat j = 0; j < i ; j = j + 1) {
			if(!definido(j)) {
				//arDef[j] = false;
				arDef.Definir(j,false);
			} else {
				//ar[j] =  array[j];
				ar.Definir(j,array[j]);
				//arDef[j] = definidos[j];
				arDef.Definir(j,definidos[j]);
				//arIt[j] = iteradores[j];
				arIt.Definir(j,iteradores[j]);
			}
			
		}
		//array = ar;
		array = Arreglo<T>(ar);
		//definidos = arDef;
		definidos = Arreglo<bool>(arDef);
		//iteradores = arIt;
		iteradores = Arreglo<Conj<Nat>::Iterador>(arIt);
		largo = i+1;
	 
	}
	//iteradores[i] = cclaves.AgregarRapido(i);
	if (!definidos.Definido(i))
		iteradores.Definir(i, cclaves.AgregarRapido(i));
	//definidos[i] = true;
	definidos.Definir(i,true);
	//array[i] = A;
	array.Definir(i,A);
}

template<class T>
bool DiccNatAlpha<T>::definido(const Nat i) const{
	//std::cout << array[i] << std::endl;
	return i < largo && definidos[i];
}
template<class T>
const T& DiccNatAlpha<T>::significado(const Nat i) const {

	#ifdef DEBUG
	assert (definido(i));
	#endif

	return array[i];
}

template<class T>
void DiccNatAlpha<T>::borrar(const Nat i){

	#ifdef DEBUG
	assert (definido(i));
	#endif

	//definidos[i] = false;
	definidos.Definir(i, false);
	iteradores[i].EliminarSiguiente();
}
template<class T>
Conj<Nat>::const_Iterador DiccNatAlpha<T>::claves() const{
	return cclaves.CrearIt();
}



}
	
#endif	
	
