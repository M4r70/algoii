#include "Mapa.h"
//TODO OPERADOR ASIGNACION

Mapa::Mapa() {
	sendas = NULL;
}


Mapa::~Mapa() {
	if (sendas != NULL)
		delete sendas;
		
	Lista<const DiccS<Restr>* >::Iterador it1 = _iter.CrearIt(); 
	while (it1.HaySiguiente()) {
		delete it1.Siguiente();
		//it1.EliminarSiguiente();
		it1.Avanzar();
	}
}

//	DiccS<DiccS<Restr> >::Iterador it1 = DiccS<DiccS<Restr> >::Iterador(mapa);
//	while (it1.hayMas()) {
//		delete &it1.actual();
//		it1.avanzar();
//	}
//}

void Mapa::Agregar(const Estacion& e) {

	#ifdef DEBUG
	assert( !(Estaciones().Pertenece(e)) );
	#endif

	DiccS<Restr>* d2 = new DiccS<Restr>;
	mapa.definir(e, *d2);
	_iter.AgregarAtras(d2);
}

void Mapa::Conectar(const Estacion& e1, const Estacion& e2, Restr& r) {

	#ifdef DEBUG
	assert( (Estaciones().Pertenece(e1)) && (Estaciones().Pertenece(e2)) && !Conectadas(e1, e2) );
	#endif
	
	DiccS<Restr> *d1 = &mapa.obtener(e1);
	d1->definir(e2, r);
	mapa.definir(e1, *d1);
	
	d1 = &mapa.obtener(e2);
	d1->definir(e1, r);
	mapa.definir(e2, *d1);
}

const Conj<Estacion>& Mapa::Estaciones() const{
	return mapa.claves();
}


bool Mapa::Conectadas(const Estacion& e1, const Estacion& e2) const{
	
	#ifdef DEBUG
	assert( (Estaciones().Pertenece(e1)) && (Estaciones().Pertenece(e2)) );
	#endif
	
	return (mapa.def(e1) && mapa.obtener(e1).def(e2)) 
		|| (mapa.def(e2) && mapa.obtener(e2).def(e1));
}

Restr& Mapa::Restriccion(const Estacion& e1,const Estacion& e2) const{

	#ifdef DEBUG
	assert( (Estaciones().Pertenece(e1)) && (Estaciones().Pertenece(e2)) );
	#endif

	return mapa.obtener(e1).obtener(e2);
}	

Conj<Mapa::senda>::const_Iterador Mapa::Sendas(){
	if (sendas != NULL)
		delete sendas;
	sendas = new Conj<senda>;
	Conj<Estacion>::const_Iterador it1 = Estaciones().CrearIt();
	
	while (it1.HaySiguiente()) {
		DiccS<Restr> d = mapa.obtener(it1.Siguiente());
		Conj<Estacion>::const_Iterador it2 = d.claves().CrearIt();
		while (it2.HaySiguiente()) {
			senda s = senda(it1.Siguiente(), it2.Siguiente(), d.obtener(it2.Siguiente()));
			sendas->AgregarRapido(s);
			it2.Avanzar();
		}
	it1.Avanzar();
	}
	
	return sendas->CrearIt();
}

bool Mapa::senda::operator==(const senda& otro) const{
	bool i = false;
	if (&this->r == &otro.r){
		if (this->e1 == otro.e1 && this->e2 == otro.e2){
			i = true;
		}
		if (this->e2 == otro.e1 && this->e1 == otro.e2){
			i = true;
		}
	}
	return i;
}

bool Mapa::senda::operator!=(const senda& otro) const{
	return !(*this == otro);
}
