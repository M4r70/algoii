/*
 * Author: MathiaZ
 *
 * El CPP De Restriccion .
 * 
 */

#include "Restr.h"

using namespace aed2;

//METODOS DE RESTRICCION

Restr::Restr(const String& str){
	//asumiendo que str es un string valido.
	arbol = ArbolSintactico::LeerDeString(str);
}

Restr::Restr(const Restr& r){
	//asumiendo que str es un string valido.
	arbol = ArbolSintactico::LeerDeString(r.arbol->aString());
}

bool Restr::verifica(const conjS& tags) const{
	return verificaAux(arbol,tags);	
}

bool Restr::verificaAux(ArbolSintactico* ar, const conjS& tags) const{
	bool resultado; 
	if( (ar->der == NULL) && (ar->izq == NULL) ){
		resultado = tags.pertenece(ar->raiz);
	}else if(ar->raiz == "!"){
		resultado = !(verificaAux(ar->izq, tags)); 
	}else if(ar->raiz == "&"){
		resultado = verificaAux(ar->izq, tags) && verificaAux(ar->der, tags);  
	}else{
		resultado = verificaAux(ar->izq, tags) || verificaAux(ar->der, tags);
	}
	return resultado;
}

Restr::~Restr(){
	delete arbol;
}

String Restr::verRestr() const{
	return arbol->aString();
}
