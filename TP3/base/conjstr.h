#ifndef CONJ_STR
#define CONJ_STR

#include "diccs.h"
#include "aed2.h"

class conjS{
public:
	//conjS();
	~conjS();
	
	void agregar(const String& s);
	bool pertenece(const String& s) const;
	const Conj<String>& claves() const;
	conjS& operator=(const conjS& otro);
	
private:
	DiccS<bool> dicc;
	Lista<bool*> lst;
};

/*conjS::conjS(){
	
}*/

#endif

