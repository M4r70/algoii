#include "conjstr.h"

conjS& conjS::operator=(const conjS& otro){
	dicc = otro.dicc;
	return *this;
}


conjS::~conjS(){
	Lista<bool*>::Iterador it = lst.CrearIt();
	while(it.HaySiguiente()){
		delete it.Siguiente();
		it.Avanzar();
	}
}

void conjS::agregar(const String& s){
	bool* nuevo = new bool(true);
	dicc.definir(s, *nuevo);
	lst.AgregarAtras(nuevo);
}

bool conjS::pertenece(const String& s) const{
	return dicc.def(s);
}

const Conj<String>& conjS::claves() const{
	return dicc.claves();
}
