#include "Driver.h"


namespace aed2 {

Driver::Driver(const Conj<Estacion> &estacionesIniciales) {
    
	Conj<Estacion>::const_Iterador it = estacionesIniciales.CrearIt();
	map = new Mapa();
	
	while(it.HaySiguiente()){
		map->Agregar(it.Siguiente());
		it.Avanzar();
	}
	ciudad = new Ciudad(*map);
}

Driver::~Driver() {
    
    delete ciudad;
    delete map;
    
    Lista<Restr*>::const_Iterador itH4x = l337h4xZ.CrearIt();
	while(itH4x.HaySiguiente()) {
		delete itH4x.Siguiente();
 		itH4x.Avanzar();
 	}
}

Nat Driver::CantidadEstaciones() const
{
	Conj<Estacion>::const_Iterador itEstaciones = ciudad->estaciones();
	Nat i = 0;
	while(itEstaciones.HaySiguiente()) {
 		i++;
 		itEstaciones.Avanzar();
 	}
	return i;
 
}

// PRE: i < #estaciones()
const Estacion& Driver::IesimaEstacion(Nat i) const
{
	#ifdef DEBUG
	assert( i < CantidadEstaciones());
	#endif
	
	Conj<Estacion>::const_Iterador itEs = ciudad->estaciones();
	Nat j = 0 ;
	while(itEs.HaySiguiente() && j < i) {
		j++;
		itEs.Avanzar();
	}
	return itEs.Siguiente();
}

// PRE: e \in estaciones()
Nat Driver::CantidadDeSendasParaEstacion(const Estacion &e) const {
	
	#ifdef DEBUG
	assert( map->Estaciones().Pertenece(e));
	#endif
	
	Conj<Estacion>::const_Iterador itEs = map->Estaciones().CrearIt() ;
	Nat i = 0 ;
	while(itEs.HaySiguiente()) {
		if (e != itEs.Siguiente()){
			if(map->Conectadas(e,itEs.Siguiente())){
				i++;
			}
		}
		itEs.Avanzar();
	}
	return i;
}

// PRE: e \in estaciones() \and i < # {e2:Estacion | conectadas?(e,e2)}
const Estacion& Driver::IesimaEstacionDeSenda(const Estacion &e, Nat i) const
{
	#ifdef DEBUG
	Nat CantEstaciones = 0;
	Conj<Mapa::senda>::const_Iterador iter = map->Sendas();
	
	while(iter.HaySiguiente()){
		if (iter.Siguiente().e1 == e){
			CantEstaciones++;
		}
		iter.Avanzar();
	}
	
	assert( map->Estaciones().Pertenece(e) && i < CantEstaciones);
	#endif
	
	Conj<Mapa::senda>::const_Iterador it = map->Sendas();
	Conj<const Estacion*> cjs;
	Conj<const Estacion*>::const_Iterador it2;
	
	while(it.HaySiguiente()){
		if (it.Siguiente().e1 == e){
			cjs.AgregarRapido(&(it.Siguiente().e2));
		}
		it.Avanzar();
	}
	
	it2 = cjs.CrearIt();
	Nat j = 0;
	while(it2.HaySiguiente() && j<i){
		it2.Avanzar();
		j++;
	}
	
	return *it2.Siguiente();
}

// PRE: e \in estaciones() \and i < # {e2:Estacion | conectadas?(e,e2)}
const Restriccion& Driver::IesimaRestriccionDeSenda(const Estacion &e1, Nat i) const
{
	#ifdef DEBUG
	Nat CantEstaciones = 0;
	Conj<Mapa::senda>::const_Iterador iter = map->Sendas();
	
	while(iter.HaySiguiente()){
		if (iter.Siguiente().e1 == e1){
			CantEstaciones++;
		}
		iter.Avanzar();
	}
	
	assert( map->Estaciones().Pertenece(e1) && i < CantEstaciones);
	#endif
	
	Conj<Mapa::senda>::const_Iterador it = map->Sendas();
	Conj<const Restriccion*> cjs;
	Conj<const Restriccion*>::const_Iterador it2;
	
	while(it.HaySiguiente()){
		if (it.Siguiente().e1 == e1){
			cjs.AgregarRapido(&(it.Siguiente().s));
		}
		it.Avanzar();
	}
	
	it2 = cjs.CrearIt();
	Nat j = 0;
	while(it2.HaySiguiente() && j<i){
		it2.Avanzar();
		j++;
	}
	
	return *it2.Siguiente();
}

// PRE: e1 \in estaciones() \and e2 \in estaciones() \and \not conectadas?(e1,e2)
void Driver::AgregarSenda(const Estacion &e1, const Estacion &e2, Restriccion r)
{
	#ifdef DEBUG
	assert( map->Estaciones().Pertenece(e1) && map->Estaciones().Pertenece(e2) 
		&& !map->Conectadas(e1, e2));
	#endif
	
    Restr* erre = new Restr(r);
    l337h4xZ.AgregarAtras(erre);
    map->Conectar(e1, e2, *erre);
    
}

Nat Driver::CantidadRobotsActivos() const{
	Conj<RUR>::const_Iterador rs = ciudad->robots();
	Nat i = 0;
	while(rs.HaySiguiente()){
		i++;
		rs.Avanzar();
	}
	return i ;	
 
}

/// PRE: i < #robots()
RUR Driver::IesimoRobotActivo(Nat i) const
{
	 #ifdef DEBUG
	 assert(i < CantidadRobotsActivos());
	 #endif
	 
	 Conj<RUR>::const_Iterador rs = ciudad->robots();
	 Nat j = 0;
	 while(j < i) {
	 	j++;
	 	rs.Avanzar();
	 }
	 return rs.Siguiente();
	 
}

/// PRE: i < #robots()
const Estacion& Driver::EstacionActualIesimoRobotActivo(Nat i) const
{
 	 #ifdef DEBUG
	 assert(i < CantidadRobotsActivos());
	 #endif
	 
	 RUR bot = IesimoRobotActivo(i);
	 return ciudad->estacion(bot);
 
}

/// PRE: i < #robots()
const Conj<Caracteristica>& Driver::CaracteristicasIesimoRobotActivo(Nat i) const
{
	 #ifdef DEBUG
	 assert(i < CantidadRobotsActivos());
	 #endif
	 
	 RUR bot = IesimoRobotActivo(i);
	 return ciudad->tags(bot).claves();
}

/// PRE: i < #robots()
Nat Driver::CantInfraccionesIesimoRobotActivo(Nat i) const
{
	 #ifdef DEBUG
	 assert(i < CantidadRobotsActivos());
	 #endif
	 
	 RUR bot = IesimoRobotActivo(i);
	 return ciudad->infracciones(bot);
}

/// PRE: (\exists r:RUR) r \in robots() \and #infracciones(r) > 0
RUR Driver::ElMasInfractor() const
{
	 #ifdef DEBUG
	 bool SI = false;
	 Conj<RUR>::const_Iterador itRs = ciudad->robots();
	 while (itRs.HaySiguiente()){
	 	if (ciudad->infracciones(itRs.Siguiente())){
	 		SI = true;
	 		break;
	 	}
	 	itRs.Avanzar();
	 }
	 assert(CantidadRobotsActivos() > 0 && SI);
	 #endif
	 
	 Conj<RUR>::const_Iterador rs = ciudad->robots();
	 RUR publicEnemyNmbrOne = rs.Siguiente();
	 while(rs.HaySiguiente()) {
	 	if(ciudad->infracciones(rs.Siguiente()) > ciudad->infracciones(publicEnemyNmbrOne)) {publicEnemyNmbrOne = rs.Siguiente();}
	 	rs.Avanzar();
	 }
	 return publicEnemyNmbrOne;
}

/// PRE: estacionInicial \in estaciones()
void Driver::Entrar(const Conj<Caracteristica> &cs, const Estacion &estacionInicial)
{
	#ifdef DEBUG
	assert(map->Estaciones().Pertenece(estacionInicial));
	#endif
	
	conjS csS;
	Conj<Caracteristica>::const_Iterador itCs = cs.CrearIt();
	while(itCs.HaySiguiente()) {
		csS.agregar(itCs.Siguiente());
		itCs.Avanzar();
	}
	
 	ciudad->entrar(csS, estacionInicial) ;
}

/// PRE: robot \in robots() \and destino \in estaciones()
void Driver::Mover(RUR robot, const Estacion &destino)
{
	 #ifdef DEBUG
	 bool SI = false;
	 Conj<RUR>::const_Iterador itRs = ciudad->robots();
	 while (itRs.HaySiguiente()){
	 	if (itRs.Siguiente() == robot){
	 		SI = true;
	 		break;
	 	}
	 	itRs.Avanzar();
	 }
	 assert(SI && map->Estaciones().Pertenece(destino));
	 #endif
	
	 ciudad->mover(robot,destino);
}

/// PRE: e \in estaciones()
void Driver::Inspeccion(const Estacion &e)
{
	#ifdef DEBUG
	assert(map->Estaciones().Pertenece(e));
	#endif
	
	ciudad->inspeccion(e);
}

} // namespace aed2


