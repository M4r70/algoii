#ifndef CDP_INCLUDED
#define CDP_INCLUDED

#include <iostream> // quitar después de testear

// Módulo Cola de Prioridad(alpha)

template <class T> 
class cdp{
public:
	
	// Constructor y destructor
	cdp();
	~cdp();
	
	// Forward declaration del iterador
	class itCola;
	
	// Operaciones básicas de la Cola de Prioridad
	itCola encolar(const T&);		// Encola un elemento
	bool vacia() const;				// Verifica si la cola está vacía
	const T& proximo() const;		// Devuelve el próximo elemento
	itCola desencolar();			// Desencola el próximo elemento
	
	itCola crearIt();				// Creación del iterador
	
	// iterador
	class itCola{
	public:
	
		itCola(); // Constructor por defecto
		
		// Devuelve una referencia al elemento actual del iterador
		const T& actual() const;
		
		// Elimina el actual del iterador
		// EXTRA: El iterador no se invalida mientras haya elementos en la cola
		// El nuevo "actual" será el último de la cola
		void itEliminar();			
		
	private:
		// Estructura interna del iterador
		typename cdp<T>::Nodo *n;
		cdp *col;
		
		itCola(cdp<T>* cola, typename cdp<T>::Nodo* proximo);
		
		friend void cdp<T>::setN(itCola&, typename cdp<T>::Nodo*); //Setea n
		
		friend typename cdp<T>::itCola cdp<T>::crearIt(); // Crea un iterador de cola
		
	};
	
private:
	//Estructura interna
	
	struct Nodo{
		Nodo(const T& cl){
			clave = cl;
			i = NULL;
			d = NULL;
			padre = NULL;
			
		};
		
		~Nodo(){
			
			if (padre != NULL){
				
				if (padre->i == this)
					padre->i = NULL;
				else
					padre->d = NULL;
					
				padre = NULL;
			}
			
			if (i != NULL)
				delete i;
			if (d != NULL)
				delete d;
		}
		
		T clave;
		Nodo *i;
		Nodo *d;
		Nodo *padre;
	};
	
	Nodo* primero;
	Nodo* ultimo;
	
	//swap de nodos
	void swap(Nodo*, Nodo*);
	//muestra un nodo
	void mostrarNodo(Nodo *n);
	//Cambia n para un itCola
	void setN(itCola&, typename cdp<T>::Nodo*);
};

// No creo que necesitemos sobrecargar esto...
template<class T>
std::ostream& operator << (std::ostream& os, const cdp<T>& c);

//implementación

template<typename T>
cdp<T>::cdp() : primero(NULL), ultimo(NULL){};

template<typename T>
cdp<T>::~cdp(){
	while(!vacia()){
		desencolar();
	}
}

// Encola un elemento
template<typename T>
typename cdp<T>::itCola cdp<T>::encolar(const T& e){
	
	Nodo *nuevo = new Nodo(e);
	Nodo *nodoBuf = ultimo;
	
	if (primero == NULL){
		primero = nuevo;
		ultimo = nuevo;
	} else if (ultimo->padre == NULL){
		ultimo->i = nuevo;
		nuevo->padre = ultimo;
	} else if (ultimo->padre->d == NULL){
		ultimo->padre->d = nuevo;
		nuevo->padre = ultimo->padre; // modificado
	} else{
		//Subo buscando
		while(nodoBuf->padre != NULL){
			if (nodoBuf->padre->d != nodoBuf){
				nodoBuf = nodoBuf->padre->d;
				break;
			} //modificado
			nodoBuf = nodoBuf->padre;
		}
		
		//bajo por izquierda
		while(nodoBuf->i != NULL){
			nodoBuf = nodoBuf->i;
		}
		
		nuevo->padre = nodoBuf;
		nodoBuf->i = nuevo;
	}
	
	nodoBuf = nuevo;
	ultimo = nodoBuf; //Agregado
	while(nodoBuf->padre != NULL){
		//swaps
		if (nodoBuf->clave > nodoBuf->padre->clave){
			//Agregado porque no actualizabamos primero y ultimo
			if (ultimo == nodoBuf){
				ultimo = nodoBuf->padre;
			}
			
			if (primero == nodoBuf->padre){
				primero = nodoBuf;
			}
			
			swap(nodoBuf, nodoBuf->padre);
			//swap(nodoBuf, nodoBuf->padre); MAL
		} else{
			break;
		}
	}
	
	itCola it = crearIt();
	setN(it, nuevo);
	
	//debug
	//std::cout << "primero: \n";
	//mostrarNodo(primero);
	//std::cout << "ultimo: \n";
	//mostrarNodo(ultimo);
	
	return it;
	
}

// Verifica si la cola está vacía
template<typename T>
bool cdp<T>::vacia() const{
	return primero == NULL;
}

// Devuelve el próximo elemento
template<typename T>
const T& cdp<T>::proximo() const{
	#ifdef DEBUG
	assert( !vacia() );
	#endif
	
	return primero->clave;
}

// Desencola el próximo elemento
template<typename T>
typename cdp<T>::itCola cdp<T>::desencolar(){
	#ifdef DEBUG
	assert( !vacia() );
	#endif
	
	itCola it = crearIt();
	it.itEliminar();
	return it;
}

template<typename T>
void cdp<T>::mostrarNodo(Nodo *n){
	std::cout << "\t clave: " << n->clave << "\n";
	
	std::cout << "\t padre: ";
	if (n->padre != NULL)
		std::cout << n->padre->clave << "\n";
	else 
		std::cout << "NULL\n";
		
	std::cout << "\t izq: ";
	if (n->i != NULL){
		std::cout << n->i->clave << "\n";
		if (n->i->padre != NULL)
			std::cout << "\t\t" << n->i->padre->clave << "\n";
		else 
			std::cout << "\t\tNULL\n";
	}	
	else 
		std::cout << "NULL\n";
		
	std::cout << "\t der: ";
	if (n->d != NULL){
		std::cout << n->d->clave << "\n";
		if (n->d->padre != NULL)
			std::cout << "\t\t" << n->d->padre->clave << "\n";
		else 
			std::cout << "\t\tNULL\n";
	}	
	else 
		std::cout << "NULL\n";
}

// Modificado
template<typename T>
void cdp<T>::swap(Nodo* c1, Nodo* c2){
	#ifdef DEBUG
	assert( c1 != NULL && c2 != NULL );
	#endif
	/* debug
	std::cout << "-------------------\n";
	std::cout << "c1: \n";
	mostrarNodo(c1);
	std::cout << "c2: \n";
	mostrarNodo(c2);
	std::cout << "--------SWAP!------\n";
	*/
	
	Nodo* buf;
	
	if (c2->padre == c1){
		
		//Cambio del hijo del padre de c1
		if (c1->padre != NULL) {
			if (c1->padre->i == c1)
				c1->padre->i = c2;
			else{
				c1->padre->d = c2;
			}
		}
		
		//Cambio los padres
		c2->padre = c1->padre;
		c1->padre = c2;
		
		if (c1->i == c2){
			c1->i = c2->i;
			c2->i = c1;
			
			buf = c1->d;
			c1->d = c2->d;
			c2->d = buf;
			
		} else {
			c1->d = c2->d;
			c2->d = c1;
			
			buf = c1->i;
			c1->i = c2->i;
			c2->i = buf;
		}
		
		
	} else if (c1->padre == c2){
		
		//Cambio del hijo del padre de c2
		if (c2->padre != NULL) {
			if (c2->padre->i == c2)
				c2->padre->i = c1;
			else{
				c2->padre->d = c1;
			}
		}
		
		//Cambio los padres
		c1->padre = c2->padre;
		c2->padre = c1;
		
		if (c2->i == c1){
			c2->i = c1->i;
			c1->i = c2;
			
			buf = c2->d;
			c2->d = c1->d;
			c1->d = buf;
			
		} else {
			c2->d = c1->d;
			c1->d = c2;
			
			buf = c2->i;
			c2->i = c1->i;
			c1->i = buf;
		}
		
	} else {
	
		//Cambio al hijo del padre de c1
		if (c1->padre != NULL && c2->padre != NULL){
			if ((c1->padre->i == c1 && c1->padre->d == c2) 
			|| (c1->padre->d == c1 && c1->padre->i == c2)){
			
				buf = c1->padre->i;
				c1->padre->i = c1->padre->d;
				c1->padre->d = buf;
				
			} else {
				
				if (c1->padre->i == c1)
					c1->padre->i = c2;
				if (c1->padre->d == c1)
					c1->padre->d = c2;
					
				if (c2->padre->i == c2)
					c2->padre->i = c1;
				if (c2->padre->d == c2)
					c2->padre->d = c1;
			}
		} else {
			if (c1->padre != NULL){
				if (c1->padre->i == c1)
					c1->padre->i = c2;
				if (c1->padre->d == c1)
					c1->padre->d = c2;
			}
			if (c2->padre != NULL) {
				if (c2->padre->i == c2)
					c2->padre->i = c1;
				if (c2->padre->d == c2)
					c2->padre->d = c1;
			}
		}
				
		//Cambio los padres
		buf = c1->padre;
		c1->padre = c2->padre;
		c2->padre = buf;
			
		buf = c2->d;
		c2->d = c1->d;
		c1->d = buf;
			
		buf = c2->i;
		c2->i = c1->i;
		c1->i = buf;
		
	}
	
	// Cambio los padres de los hijos
	if (c1->i != NULL)
		c1->i->padre = c1;
	if (c1->d != NULL)
		c1->d->padre = c1;
	if (c2->i != NULL)
		c2->i->padre = c2;
	if (c2->d != NULL)
		c2->d->padre = c2;	
	
	/* debug
	std::cout << "c1: \n";
	mostrarNodo(c1);
	std::cout << "c2: \n";
	mostrarNodo(c2);
	std::cout << "--------FIN--------\n";
	*/
	
}

// ----- Implementacion iterador -----

// Crea un iterador de cola
template <typename T>
typename cdp<T>::itCola cdp<T>::crearIt(){
	#ifdef DEBUG
	assert( !vacia() );
	#endif
	return itCola(this, primero);
}

// Devuelve una referencia al elemento actual del iterador
template <typename T>
const T& cdp<T>::itCola::actual() const{
	#ifdef DEBUG
	assert( !col->vacia() );
	#endif
	return n->clave;
}

// Elimina el actual del iterador
template <typename T>
void cdp<T>::itCola::itEliminar(){
	
	#ifdef DEBUG
	assert( !col->vacia() );
	#endif
	
	/* debug
	std::cout << "primero: \n";
	col->mostrarNodo(col->primero);
	std::cout << "ultimo: \n";
	col->mostrarNodo(col->ultimo);
	*/	
	
	//Si fuera el único elemento
	if (col->ultimo == n){
		
		if (n->padre != NULL){
			
			Nodo *buf = n->padre;
			Nodo* NodoBuf = buf;
			
			//busco el nuevo último
			if (buf->d == n){
				buf = buf->i;
				buf->d = NULL;
			} else if (buf->d != NULL){
				buf = buf->d;
				buf->i = NULL;
			} else {
				//subo, fijándome si vine por derecha
				while(buf->padre != NULL){
					// Si vengo por derecha, voy al nodo de la izquierda
					// Siempre hay, porque es izquierdista
					if (buf->padre->d == buf){
						buf = buf->padre->i;
						break;
					}
					buf = buf->padre;
				}
				// bajo por derecha
				while(buf->d != NULL){
					buf = buf->d;
				}
				n->padre->i = NULL;
			}
			
			col->ultimo = buf;
			delete n;
			n = NodoBuf;
			
			
		} else {
			col->primero = NULL; //nos olvidamos de esto
			col->ultimo = NULL;
			delete n;
			n = NULL;
		}
		
	} else { // si no
		Nodo *nodoBuf;
		
		col->swap(n, col->ultimo);
		//n es el último ahora
		
		Nodo *buf = n->padre;
		//busco el nuevo último
		if (buf->d == n){
			buf = buf->i;
		} else {
			//subo, fijándome si vine por derecha
			while(buf->padre != NULL){
				// Si vengo por derecha, voy al nodo de la izquierda
				// Siempre hay, porque es izquierdista
				if (buf->padre->d == buf){
					buf = buf->padre->i;
					break;
				}
				buf = buf->padre;
			}
			// bajo por derecha
			while(buf->d != NULL){
				buf = buf->d;
			}
		}
		
		//if (n->padre->d == n)
		//	n->padre->d = NULL;
		//if (n->padre->i == n)
		//	n->padre->i = NULL;
		delete n; //elimino el nodo
		nodoBuf = col->ultimo;
		
		col->ultimo = buf;
		
		//faltó chequeo del primero
		if (nodoBuf->padre == NULL)
			col->primero = nodoBuf;
		
		//Nos fijamos si es necesario subir y de ser así, subimos
		while(nodoBuf->padre != NULL){
			if(nodoBuf->padre->clave < nodoBuf->clave){
			
				if (nodoBuf == buf){
					col->ultimo = nodoBuf->padre;
				}
				
				col->swap(nodoBuf->padre, nodoBuf);
				
				if (nodoBuf->padre == NULL) //faltó chequeo del primero
					col->primero = nodoBuf;
			} else {
				break;
			}
		}
		
		//Faltó el caso de si hay dos nomás
		if (nodoBuf->i == NULL && nodoBuf->d == NULL){
			n = nodoBuf;
			return;
		}
		
		//Nos fijamos si es necesario bajar y de ser así, bajamos
		while(nodoBuf->i != NULL || nodoBuf->d != NULL){
		
			if(nodoBuf->i != NULL && nodoBuf->d != NULL){
				if (nodoBuf->i->clave > nodoBuf->clave && nodoBuf->d->clave > nodoBuf->clave){
					if (nodoBuf->i->clave >= nodoBuf->d->clave){
						if (nodoBuf->padre == NULL) //faltó chequeo del primero
							col->primero = nodoBuf->i;
						
						if (nodoBuf->i == col->ultimo){
							col->ultimo = nodoBuf; //Estaba mal
						}
						
						col->swap(nodoBuf, nodoBuf->i);
						
					} else {
					
						if (nodoBuf->padre == NULL) //faltó chequeo del primero
							col->primero = nodoBuf->d;

						if (nodoBuf->d == col->ultimo){
							col->ultimo = nodoBuf; //Estaba mal
						}
						
						col->swap(nodoBuf, nodoBuf->d);
					}
				} else if (nodoBuf->i->clave >= nodoBuf->clave){
					
					if (nodoBuf->padre == NULL) //faltó chequeo del primero
						col->primero = nodoBuf->i;

					if (nodoBuf->i == col->ultimo){
						col->ultimo = nodoBuf; //Estaba mal
					}
					
					col->swap(nodoBuf, nodoBuf->i);
					
				} else if (nodoBuf->d->clave >= nodoBuf->clave){
				
					if (nodoBuf->padre == NULL) //faltó chequeo del primero
						col->primero = nodoBuf->d;
							
					if (nodoBuf->d == col->ultimo){
						col->ultimo = nodoBuf; //Estaba mal
					}
					
					col->swap(nodoBuf, nodoBuf->d);
					
				} else {
					break;
				}
				
			} else if (nodoBuf->i != NULL){
				if (nodoBuf->i->clave > nodoBuf->clave) {
					
					if (nodoBuf->padre == NULL) //faltó chequeo del primero
						col->primero = nodoBuf->i;
	
					if (nodoBuf->i == col->ultimo) {
						col->ultimo = nodoBuf; //Estaba mal
					}
					
					col->swap(nodoBuf, nodoBuf->i);
					
				} else {
					break;
				}
			} else {
				if (nodoBuf->d->clave > nodoBuf->clave) {
					
					if (nodoBuf->padre == NULL) //faltó chequeo del primero
						col->primero = nodoBuf->d;

					if (nodoBuf->d == col->ultimo) {
						col->ultimo = nodoBuf; //Estaba mal
					}
					
					col->swap(nodoBuf, nodoBuf->d);
					
				} else {
					break;
				}
			}
			
		} //end while
		n = nodoBuf;
		
	}
}

template <typename T>
cdp<T>::itCola::itCola() : n(NULL), col(NULL){}

template <typename T>
cdp<T>::itCola::itCola(cdp<T>* cola, typename cdp<T>::Nodo* proximo) : n(proximo), col(cola) {}

template <typename T>
void cdp<T>::setN(typename cdp<T>::itCola& it, typename cdp<T>::Nodo* nuevo){
	it.n = nuevo;
}


#endif
