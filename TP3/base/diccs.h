//Diccionario String Alpha.
//Template.

#ifndef DICCS
#define DICCS

#include "aed2.h"

template<typename Alpha>
class DiccS{
	public:
		//Diccionario.
		DiccS();	//Crear.
		void definir(const String& s, Alpha& x);
		Alpha& obtener(const String& s) const;
		bool def(const String& s) const;

		~DiccS();
		DiccS(const DiccS<Alpha>& otro);
		DiccS<Alpha>& operator=(const DiccS<Alpha>& otro);
		
		const Conj<String>& claves() const;

		//Iterador de significados.
		class Iterador;

		class Iterador{
			public:
				Iterador(const DiccS<Alpha>& d);	//CrearItDiccS.
				bool hayMas() const;
				const Alpha& actual() const;
				void avanzar();

			private:
				typename Lista<Alpha**>::const_Iterador _itLista;
		};

	private:
		//Trie.
		DiccS<Alpha>* _arr[256];	//Arreglo de punteros a DiccS (estructura recursiva).
		Alpha* _significado;
		Conj<String> _claves;
		//Estructura de iteracion.
		Lista<Alpha**> _iter;
};

//Implementacion de DiccS.
template<typename Alpha>
DiccS<Alpha>::DiccS(){ //Crear.
	for(int i = 0; i < 256; i++)
		_arr[i] = NULL;
	_significado = NULL;
}

template<typename Alpha>
DiccS<Alpha>::DiccS(const DiccS<Alpha>& otro) : _iter(otro._iter) {
	for(int i = 0; i < 256; i++){
		if(otro._arr[i] != NULL){
			_arr[i] = new DiccS<Alpha>(*(otro._arr[i]));
		} else {
			_arr[i] = NULL;
		}
	}
	_significado = otro._significado;
	_claves = Conj<String>(otro._claves);
}

template<typename Alpha>
DiccS<Alpha>& DiccS<Alpha>::operator=(const DiccS<Alpha>& otro){
	//Destruir.
	for(int i = 0; i < 256; i++){
		if(_arr[i] != NULL)
			delete _arr[i];
			_arr[i] = NULL;
	}

	//Construir.
	for(int i = 0; i < 256; i++){
		if(otro._arr[i] != NULL){
			_arr[i] = new DiccS<Alpha>(*(otro._arr[i]));
		} else {
			_arr[i] = NULL;
		}
	}
	_significado = otro._significado;
	_iter = otro._iter;
	_claves = Conj<String>(otro._claves);
	
	return *this;
}

template<typename Alpha>
DiccS<Alpha>::~DiccS(){
	for(int i = 0; i < 256; i++){
		if(_arr[i] != NULL)
			delete _arr[i];
			_arr[i] = NULL;
	}
}

template<typename Alpha>
void DiccS<Alpha>::definir(const String& s, Alpha& x){
	bool estabaDefinido = def(s);
	
	DiccS<Alpha>* otraEstr = this;
	for(Nat i = 0; i < s.length(); i++){
		DiccS<Alpha>** arreglo = otraEstr->_arr;
		DiccS<Alpha>* puntero = arreglo[(unsigned int)s[i]];
		if(puntero != NULL){
			otraEstr = puntero;
		} else {
			DiccS<Alpha>* ptrNuevaEstr = new DiccS();
			arreglo[(unsigned int)s[i]] = ptrNuevaEstr;
			otraEstr = ptrNuevaEstr;
		}
	}
	otraEstr->_significado = &x;	//Por referencia

	if(!estabaDefinido){
		_iter.AgregarAtras(&(otraEstr->_significado));
		_claves.AgregarRapido(s);
	}
		
}

template<typename Alpha>
Alpha& DiccS<Alpha>::obtener(const String& s) const{

	#ifdef DEBUG
	assert (def(s));
	#endif

	DiccS<Alpha>* otraEstr = _arr[(unsigned int)s[0]];
	
	for(Nat i = 1; i < s.length(); i++){
		DiccS<Alpha>** arreglo = otraEstr->_arr;
		otraEstr = arreglo[(unsigned int)s[i]];
	}
	return *(otraEstr->_significado);
}

template<typename Alpha>
bool DiccS<Alpha>::def(const String& s) const{
	DiccS<Alpha>* puntero = _arr[(unsigned int)s[0]];
	if(puntero == NULL)
		return false;
	
	DiccS<Alpha>* otraEstr = puntero;

	for(Nat i = 1; i < s.length(); i++){
		DiccS<Alpha>** arreglo = otraEstr->_arr;
		DiccS<Alpha>* puntero = arreglo[(unsigned int)s[i]];
		
		if(puntero == NULL)
			return false;
			
		otraEstr = puntero;
	}
	if (otraEstr->_significado == NULL)
		return false;
	
	return true;
}

template<typename Alpha>
const Conj<String>& DiccS<Alpha>::claves() const{
	return _claves;
}

//Implementacion de IteradorDiccS.
template<typename Alpha>
DiccS<Alpha>::Iterador::Iterador(const DiccS<Alpha>& d){
	_itLista = (d._iter).CrearIt();
}

template<typename Alpha>
bool DiccS<Alpha>::Iterador::hayMas() const{
	return _itLista.HaySiguiente();
}

template<typename Alpha>
const Alpha& DiccS<Alpha>::Iterador::actual() const{
	return **(_itLista.Siguiente());
}

template<typename Alpha>
void DiccS<Alpha>::Iterador::avanzar(){

	#ifdef DEBUG
	assert (hayMas());
	#endif
	
	_itLista.Avanzar();
}

#endif
