#ifndef CIUDAD_INCLUDED
#define CIUDAD_INCLUDED

#include "Tipos.h"
#include "cdp.h"
#include "diccs.h"
#include "conjstr.h"
#include "Mapa.h"
#include "diccNatAlpha.h"
#include "ArbolSintactico.h"
#include "Restr.h"

class Ciudad{
public:
	//Crea una ciudad nueva a partir del mapa m
	Ciudad(Mapa&);
	//Destructor
	~Ciudad();
	
	//Ingresa un nuevo robot en la estación e con el conjunto de tags ts
	void entrar(const conjS&, const Estacion&);
	//Mueve un robot a la estación pasada como parámetro
	void mover(RUR, const Estacion&);
	//Realiza una inspeción en la estación e y elimina al robot con mayor cantidad de infracciones de la misma
	void inspeccion(const Estacion&);
	//Devuelve la próxima RUR
	Nat proximaRUR() const;
	//Devuelve una referencia al mapa de la ciudad
	const Mapa& mapa() const;
	//Devuelve un iterador no modificable al conjunto de robots en circulación
	Conj<RUR>::const_Iterador robots() const;
	//Devuelve la estación en la que se encuentra el robot de RUR r
	const Estacion& estacion(RUR) const;
	//Devuelve una referencia al conjunto de tags del robot de RUR r
	const conjS& tags(RUR) const;
	//Devuelve la cantidad de infracciones del robot de RUR r
	Nat infracciones(RUR) const;
	//Devuelve un iterador no modificable al conjunto de estaciones de la ciudad
	Conj<Estacion>::const_Iterador estaciones() const; 

private:
	
	struct tuplaNN{
		Nat pi1;
		Nat pi2;
		
		bool operator < (const tuplaNN& otro) const{
			return !(*this > otro);
		}

		bool operator > (const tuplaNN& otro) const{
			if (pi2 == otro.pi2){
				return pi1 < otro.pi1; 
			} else{
				return pi2 > otro.pi2;
			}
		}
		
		bool operator >= (const tuplaNN& otro) const{
			if (pi2 == otro.pi2){
				return pi1 <= otro.pi1; 
			} else{
				return pi2 >= otro.pi2;
			}
		}
	};
	
	struct cosas{
		Estacion est;
		cdp<tuplaNN>::itCola itC;
		conjS cts;
		DiccS<DiccS<bool> > diccb;
	};
	
	bool T;  // 1337 H4XZ
	bool F; // 1337 H4XZ
	
	Mapa* plano;
	DiccS<cdp<tuplaNN> > estacionesInfraccion;
	DiccNatAlpha<cosas> Robots;
	Nat RobotsTotal;
	
	Lista<DiccS<bool>* > iter; // Agregado para eliminar los DiccS que se van creando
	
};

#endif
