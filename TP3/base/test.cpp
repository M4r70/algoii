#include "Driver.h"
#include "mini_test.h"
#include "aed2/Lista.h"
#include "aed2/Conj.h"
#include "aed2/Dicc.h"

#include <string>
#include <iostream>

using namespace aed2;
using namespace std;

Dicc<RUR,String> estacionesRobots;
Dicc<RUR,Nat> infraccionesRobots;

void ActualizarEstaciones(const Driver& caba){
	for (Nat i = 0;  i< caba.CantidadRobotsActivos(); ++i)
        estacionesRobots.Definir(caba.IesimoRobotActivo(i),caba.EstacionActualIesimoRobotActivo(i));
}

void ActualizarInfracciones(const Driver& caba){
	for (Nat i = 0;  i< caba.CantidadRobotsActivos(); ++i)
        infraccionesRobots.Definir(caba.IesimoRobotActivo(i),caba.CantInfraccionesIesimoRobotActivo(i));
}

/** 
 * Imprime un elemento a un string, en vez de a una pantalla, 
 * a través del operador << 
 */
template <typename T>
std::string to_str(const T& t)
{
	std::stringstream ss;
	ss << t;

	return ss.str();
}

/**
 * Esta función se puede utilizar para comparar dos colecciones
 * iterables que representen conjuntos, es decir, que no tengan 
 * elementos repetidos.
 */
template<typename T, typename S>
bool Comparar(const T& t, const S& s)
{
  typename T::const_Iterador it1 = t.CrearIt();
  typename S::const_Iterador it2 = s.CrearIt();

	// me fijo si tienen el mismo tamanho

	Nat len1 = 0;
	while( it1.HaySiguiente() ) {
		len1++;
		it1.Avanzar();
	}

	Nat len2 = 0;
	while( it2.HaySiguiente() ) {
		len2++;
		it2.Avanzar();
	}

	if ( len1 != len2 )
		return false;

	it1 = t.CrearIt();
	it2 = s.CrearIt();

	// me fijo que tengan los mismos elementos

	while( it1.HaySiguiente() )
	{
		bool esta = false;
		it2 = s.CrearIt();

		while( it2.HaySiguiente() ) {
		  if ( it1.Siguiente() == it2.Siguiente() ) {
			esta = true;
			break;
		  }
		  it2.Avanzar();
		}

		if ( !esta ) {
			return false;
		}
			
		it1.Avanzar();
	}
  
  return true;
}


// ---------------------------------------------------------------------

/**
 * Ejemplo de caso de test, con llamadas a las rutinas de aserción 
 * definidas en mini_test.h
 */
void test_ciudad_simple()
{
    Conj<Estacion> estaciones;
    estaciones.Agregar("Belgrano");
    estaciones.Agregar("Retiro");
    estaciones.Agregar("Martinez");

    Driver caba(estaciones);

    caba.AgregarSenda("Belgrano", "Retiro", "(trenDePasajeros | trenDeCarga) & !trenDeLaAlegria");
    caba.AgregarSenda("Martinez", "Retiro", "trenDeLaAlegria");
    caba.AgregarSenda("Martinez", "Belgrano", "trenDePasajeros");

    Conj<Caracteristica> r1, r2, r3;
    r1.Agregar("trenDePasajeros");
    r2.Agregar("trenDeCarga");
    r3.Agregar("trenDeLaAlegria");

    caba.Entrar(r1,"Belgrano"); // RUR 0
    caba.Entrar(r2,"Retiro");   // RUR 1
    caba.Entrar(r3,"Martinez"); // RUR 2

    ASSERT_EQ(caba.CantidadEstaciones(), 3);
    ASSERT_EQ(caba.CantidadRobotsActivos(), 3);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Belgrano"), 2);

    caba.Mover(0,"Retiro");    // RUR 0: 0 infracciones
    caba.Mover(0,"Martinez");  // RUR 0: 1 infracciones

    caba.Mover(1,"Belgrano");  // RUR 1: 0 infracciones
    caba.Mover(1,"Martinez");  // RUR 1: 1 infracciones

    caba.Mover(2,"Belgrano");  // RUR 2: 1 infracciones
    caba.Mover(2,"Retiro");    // RUR 2: 2 infracciones
	
	//Dicc<RUR,Nat> infraccionesRobots;
    // Chequeo infracciones iterando los robots
    for (Nat i = 0;  i< caba.CantidadRobotsActivos(); ++i)
        infraccionesRobots.Definir(caba.IesimoRobotActivo(i),caba.CantInfraccionesIesimoRobotActivo(i));

    ASSERT_EQ(infraccionesRobots.Significado(0), 1);
    ASSERT_EQ(infraccionesRobots.Significado(1), 1);
    ASSERT_EQ(infraccionesRobots.Significado(2), 2);

    ASSERT_EQ(caba.ElMasInfractor(),2);

    // Vuela un robot
    caba.Inspeccion("Retiro");
    ASSERT_EQ(caba.CantidadRobotsActivos(),2);

}

void test_ciudad_h4x()
{

    Conj<Estacion> estaciones;
    estaciones.Agregar("Belgrano");
    estaciones.Agregar("Retiro");
    estaciones.Agregar("Martinez");
    estaciones.Agregar("Tigre");
    estaciones.Agregar("Tortuguitas");
    estaciones.Agregar("Del Viso");
    estaciones.Agregar("Pilar");
    estaciones.Agregar("Escobar");
    estaciones.Agregar("Campana");
    estaciones.Agregar("Chacarita");

    Driver caba(estaciones);

    caba.AgregarSenda("Belgrano", "Retiro", "(trenDePasajeros | trenDeCarga) & !trenDeLaAlegria");
    
    //Camino de la tristeza
    caba.AgregarSenda("Belgrano", "Tigre", "trenDeLaTristeza | trenDeCarga");
    caba.AgregarSenda("Tigre", "Tortuguitas", "trenDeLaTristeza");
    caba.AgregarSenda("Tortuguitas", "Del Viso", "trenDeLaTristeza");
    caba.AgregarSenda("Del Viso", "Chacarita", "trenDeLaTristeza");
    
    caba.AgregarSenda("Martinez", "Retiro", "trenDeLaAlegria");
    caba.AgregarSenda("Martinez", "Belgrano", "trenDePasajeros");
    
    caba.AgregarSenda("Escobar", "Campana", "trenLoco & trenDeLaAlegria");
    caba.AgregarSenda("Campana", "Pilar", "trenLoco | Robin ");
    
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Belgrano"), 3);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Retiro"), 2);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Martinez"), 2);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Tigre"), 2);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Tortuguitas"), 2);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Del Viso"), 2);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Pilar"), 1);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Escobar"), 1);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Campana"), 2);
    ASSERT_EQ(caba.CantidadDeSendasParaEstacion("Chacarita"), 1);

    Conj<Caracteristica> r1, r2, r3, r4, r5, r6, r7, r8, r9,r10;
    r1.Agregar("trenDePasajeros");
    r2.Agregar("trenDeCarga");
    r3.Agregar("trenDeLaAlegria");
    r4.Agregar("trenDeLaTristeza");
    r5.Agregar("trenLoco");
    
    r6.Agregar("trenLoco");
    r6.Agregar("trenDeLaAlegria");
    
    r7.Agregar("trenDeLaMaldad");
    r7.Agregar("trenAnarcofeminista");
    
    r8.Agregar("trenDeLaAlegria");
    r8.Agregar("trenDeLaTristeza");
    r9.Agregar("Batman");
	r10.Agregar("Robin"); 
	//Entrar(conj(restr), estacion)
    caba.Entrar(r1,"Belgrano"); // RUR 0
    caba.Entrar(r2,"Retiro");   // RUR 1
    caba.Entrar(r3,"Martinez"); // RUR 2
    
    caba.Entrar(r4,"Belgrano"); // RUR 3 //Camino de la tristeza
    
    caba.Entrar(r5,"Escobar"); 	// RUR 4
    caba.Entrar(r6,"Campana"); // RUR 5
    caba.Entrar(r7,"Del Viso"); // RUR 6
    caba.Entrar(r8,"Tortuguitas"); // RUR 7
    caba.Entrar(r1,"Martinez"); // RUR 8
    caba.Entrar(r2,"Belgrano"); // RUR 9 //Camino de la tristeza con infracciones
    caba.Entrar(r9,"Martinez"); //rur 10 // es batman! y va a cometer muchas muchas infracciones
    caba.Entrar(r10,"Pilar"); //rur 11 // robin entra a la cancha

    ASSERT_EQ(caba.CantidadEstaciones(), 10);
    ASSERT_EQ(caba.CantidadRobotsActivos(), 12);

    caba.Mover(0,"Retiro");    // RUR 0: 0 infracciones
    caba.Mover(0,"Martinez");  // RUR 0: 1 infracciones

    caba.Mover(1,"Belgrano");  // RUR 1: 0 infracciones
    caba.Mover(1,"Martinez");  // RUR 1: 1 infracciones

    caba.Mover(2,"Belgrano");  // RUR 2: 1 infracciones
    caba.Mover(2,"Retiro");    // RUR 2: 2 infracciones
    
    for(int i = 0; i < 38; i++) { //Batman y Robin estan fuera de control!
    	caba.Mover(10,"Retiro");
    	caba.Mover(10,"Martinez");
    	caba.Mover(11,"Campana");
    	caba.Mover(10,"Belgrano");
    	caba.Mover(10,"Tigre");
    	caba.Mover(11,"Escobar");
    	caba.Mover(10,"Belgrano");
    	caba.Mover(11,"Campana");
    	caba.Mover(10,"Martinez");
    	caba.Mover(11,"Pilar");
    }
    
    //Belgrano, Tigre, Tortuguitas, Del Viso, Chacarita
    
    caba.Mover(3, "Tigre");
    ActualizarEstaciones(caba);
    ASSERT_EQ(estacionesRobots.Significado(3), "Tigre");
    
    caba.Mover(3, "Tortuguitas");
	ActualizarEstaciones(caba);
    ASSERT_EQ(estacionesRobots.Significado(3), "Tortuguitas");
    
    caba.Mover(3, "Del Viso");
	ActualizarEstaciones(caba);
	ASSERT_EQ(estacionesRobots.Significado(3), "Del Viso");
    
    caba.Mover(3, "Chacarita");
    ActualizarEstaciones(caba);
    ASSERT_EQ(estacionesRobots.Significado(3), "Chacarita");


    // Chequeo infracciones iterando los robots
    ActualizarInfracciones(caba);

    ASSERT_EQ(infraccionesRobots.Significado(0), 1);
    ASSERT_EQ(infraccionesRobots.Significado(1), 1);
    ASSERT_EQ(infraccionesRobots.Significado(2), 2);
    ASSERT_EQ(infraccionesRobots.Significado(3), 0);
    ASSERT_EQ(infraccionesRobots.Significado(10), 228);
    ASSERT_EQ(infraccionesRobots.Significado(11), 76);
    ASSERT_EQ(caba.ElMasInfractor(),10);
    
    caba.Mover(9, "Tigre");
    ActualizarEstaciones(caba);
    ASSERT_EQ(estacionesRobots.Significado(9), "Tigre");
    
    caba.Mover(9, "Tortuguitas");
	ActualizarEstaciones(caba);
    ASSERT_EQ(estacionesRobots.Significado(9), "Tortuguitas");
    
    caba.Mover(9, "Del Viso");
	ActualizarEstaciones(caba);
	ASSERT_EQ(estacionesRobots.Significado(9), "Del Viso");
    
    caba.Mover(9, "Chacarita");
    ActualizarEstaciones(caba);
    ASSERT_EQ(estacionesRobots.Significado(9), "Chacarita");
    
    ActualizarInfracciones(caba);
    ASSERT_EQ(infraccionesRobots.Significado(0), 1);
    ASSERT_EQ(infraccionesRobots.Significado(1), 1);
    ASSERT_EQ(infraccionesRobots.Significado(2), 2);
    ASSERT_EQ(infraccionesRobots.Significado(3), 0);
    ASSERT_EQ(infraccionesRobots.Significado(9), 3);
    ASSERT_EQ(infraccionesRobots.Significado(10), 228);
    ASSERT_EQ(infraccionesRobots.Significado(11), 76);
    ASSERT_EQ(caba.ElMasInfractor(),10);
    
    //checkeando que Batman sea en realidad el nro 1, Batman
    
    ASSERT_EQ(caba.IesimoRobotActivo(1),10);
    ASSERT_EQ(caba.EstacionActualIesimoRobotActivo(1),"Martinez");
    //ASSERT_EQ(caba.CaracteristicasIesimoRobotActivo(1),r9); //este assert rompe todo por alguna razon, pero el bati if de abajo nos deja claro que la bati operacion funciona como deberia
    if(caba.CaracteristicasIesimoRobotActivo(1) == r9)
    	std::cout << "  Batman OK!  ";
    ASSERT_EQ(caba.CaracteristicasIesimoRobotActivo(1).Pertenece("Batman"), true);
    ASSERT_EQ(caba.CantInfraccionesIesimoRobotActivo(1),"228");
    

    // Vuelan  robots
    caba.Inspeccion("Retiro");
    caba.Inspeccion("Pilar");
    ASSERT_EQ(caba.CantidadRobotsActivos(),10);
    

}

void test_ciudad_casos_bordes(){
	
	Conj<Estacion> estaciones;
	estaciones.Agregar("Nasa");
	estaciones.Agregar("MIT");
	Driver cosas = Driver(estaciones);
	
	//QUE PASA SI AGREGO UNA SENDA CON LA ESTACION e1 = e2?
	cosas.AgregarSenda("Nasa","Nasa", "(cientificos | ingenieros) | astronautas");
	ASSERT_EQ(cosas.IesimaRestriccionDeSenda("Nasa",0), "((cientificos|ingenieros)|astronautas)");
			
}

int main(int argc, char **argv)
{
    RUN_TEST(test_ciudad_simple);
    RUN_TEST(test_ciudad_h4x);
    RUN_TEST(test_ciudad_casos_bordes);	
	/******************************************************************
	 * TODO: escribir casos de test exhaustivos para todas            *
	 * las funcionalidades del módulo.                                *
     * La interacción con el TAD Ciudad se debe hacer exclusivamente  *
	 * a través de la interfaz del driver.                            *
	 ******************************************************************/

	return 0;
}
