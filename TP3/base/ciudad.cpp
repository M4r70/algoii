#include "ciudad.h"

Ciudad::Ciudad(Mapa& m) {
	T = true;
	F = false;
	
	plano = &m;
	Conj<Estacion>::const_Iterador it = plano->Estaciones().CrearIt();
	while(it.HaySiguiente()) {
		cdp<tuplaNN>* col = new cdp<tuplaNN>;
		estacionesInfraccion.definir(it.Siguiente(),*col);
		it.Avanzar();
	}
	RobotsTotal = 0;
}

// TODO
Ciudad::~Ciudad(){
	Conj<Estacion>::const_Iterador it = plano->Estaciones().CrearIt();
	while(it.HaySiguiente()) {
		delete &estacionesInfraccion.obtener(it.Siguiente());
		it.Avanzar();
	}
	
	Lista<DiccS<bool>* >::Iterador it1 = iter.CrearIt(); 
	while (it1.HaySiguiente()) {
		delete it1.Siguiente();
		//it1.EliminarSiguiente();
		it1.Avanzar();
	}
	
}

void Ciudad::entrar(const conjS& ts, const Estacion& e1){

	#ifdef DEBUG
	assert( mapa().Estaciones().Pertenece(e1) );
	#endif

	
	cdp<tuplaNN> *cola = &(estacionesInfraccion.obtener(e1));
	Nat rur = RobotsTotal;
	RobotsTotal++;
	
	tuplaNN tupla = {rur, 0};
	cdp<tuplaNN>::itCola encola2 = cola->encolar(tupla);
	
	DiccS<DiccS<bool> > dicc1;
	Conj<Estacion>::const_Iterador it = plano->Estaciones().CrearIt();
	while(it.HaySiguiente()) {
		DiccS<bool>* dicc2 = new DiccS<bool>;
		dicc1.definir(it.Siguiente(), *dicc2);
		iter.AgregarAtras(dicc2);
		it.Avanzar();
	}
	
	Conj<Mapa::senda>::const_Iterador itM = plano->Sendas();
	while(itM.HaySiguiente()) {
		bool flag = itM.Siguiente().r.verifica(ts);
		if (flag)
			dicc1.obtener(itM.Siguiente().e1).definir(itM.Siguiente().e2, T);
		else
			dicc1.obtener(itM.Siguiente().e1).definir(itM.Siguiente().e2, F);
		itM.Avanzar();
	}
		
	cosas tup;
	tup.est = e1;
	tup.itC = encola2;
	tup.cts = ts;
	tup.diccb = dicc1;
	Robots.definir(rur,tup);
}

void Ciudad::mover(RUR r, const Estacion& e1){

	#ifdef DEBUG
	//r esta en Robots
	bool i = false;
	Conj<RUR>::const_Iterador itrobots = robots();
	while (itrobots.HaySiguiente()) {
		if (r == itrobots.Siguiente()) {
			i = true;
			break;
			}
		else { itrobots.Avanzar();}
		}
	assert( mapa().Estaciones().Pertenece(e1) && i );
	assert( mapa().Conectadas(estacion(r),e1));
	#endif	
	
	cosas data = Robots.significado(r) ; // vale esto? Seeeehh
	
	Nat inf = data.itC.actual().pi2;
	Estacion e2 = data.est;
	
	data.itC.itEliminar();
	
	if(!data.diccb.obtener(e2).obtener(e1)) {
		inf++;
	}
	
	tuplaNN tupla;
	tupla.pi1 = r;
	tupla.pi2 = inf;
	data.itC = estacionesInfraccion.obtener(e1).encolar(tupla);
	data.est = e1;
	
	Robots.definir(r, data); //Faltaba esto, duh
}

void Ciudad::inspeccion(const Estacion& e1){
	
	#ifdef DEBUG
	assert( mapa().Estaciones().Pertenece(e1) );
	#endif
	
	Robots.borrar(estacionesInfraccion.obtener(e1).proximo().pi1);
	estacionesInfraccion.obtener(e1).desencolar();
}


Nat Ciudad::proximaRUR() const{
	return RobotsTotal ;
}

const Mapa& Ciudad::mapa() const{
	return (const Mapa&) *plano ;
}

Conj<RUR>::const_Iterador Ciudad::robots() const {
	return Robots.claves();
}

const Estacion& Ciudad::estacion(RUR r) const{

	#ifdef DEBUG
	//r esta en Robots
	bool i = false;
	Conj<RUR>::const_Iterador itrobots = robots();
	while (itrobots.HaySiguiente()) {
		if (r == itrobots.Siguiente()) {
			i = true;
			break;
			}
		else { itrobots.Avanzar();}
		}
	assert (i);
	#endif
	
	return Robots.significado(r).est ;

}

const conjS& Ciudad::tags(RUR r) const{

	#ifdef DEBUG
	//r esta en Robots
	bool i = false;
	Conj<RUR>::const_Iterador itrobots = robots();
	while (itrobots.HaySiguiente()) {
		if (r == itrobots.Siguiente()) {
			i = true;
			break;
			}
		else { itrobots.Avanzar();}
		}
	assert (i);
	#endif

	return Robots.significado(r).cts; //checkear que esta bien creado el IT
}

Nat Ciudad::infracciones(RUR r) const{

	#ifdef DEBUG
	//r esta en Robots
	bool i = false;
	Conj<RUR>::const_Iterador itrobots = robots();
	while (itrobots.HaySiguiente()) {
		if (r == itrobots.Siguiente()) {
			i = true;
			break;
			}
		else { itrobots.Avanzar();}
		}
	assert (i);
	#endif	
	
	return Robots.significado(r).itC.actual().pi2;
}

Conj<Estacion>::const_Iterador Ciudad::estaciones() const{
	return plano->Estaciones().CrearIt();
}

