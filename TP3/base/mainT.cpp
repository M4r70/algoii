// Archivo de pruebas tontas
// 
// g++ mainT.cpp ArbolSintactico.cpp -o mainT -Wall -g
// valgrind -v --leak-check=full ./main
//
//

#include <iostream>
#include "Tipos.h"
//#include "cdp.h"
//#include "diccs.h"
//#include "conjstr.h"
//#include "Mapa.h"
//#include "diccNatAlpha.h"
#include "ArbolSintactico.h"
#include "Restr.h"
//#include "ciudad.h"

using namespace std;

int main(){
	Restr r = Restr("Pepito");
	cout << r.verRestr() << endl;
	
	//Prueba de conjS para claves()
	/*
	conjS c1;
	c1.agregar("pepa");
	c1.agregar("pepe");
	c1.agregar("pepi");
	c1.agregar("pepo");
	c1.agregar("pepu");
	
	Conj<String> cosas = c1.claves();
	Conj<String>::Iterador it = cosas.CrearIt();
	while(it.HaySiguiente()){
		cout << it.Siguiente() << endl;
		it.Avanzar();
	}
	*/
	
	/*
	Mapa *m = new Mapa();
	
	Estacion a = "Nevada";
	Estacion b = "Texas";
	Estacion c = "Paris";
	Estacion d = "Neverland";
	Estacion e = "Portland";
	
	Restr r1 = Restr("ballena | orangutan");
	Restr r2 = Restr("ballena & orangutan");
	Restr r3 = Restr("saltamontes");
	Restr r4 = Restr("saltamontes & ballena");
	Restr r5 = Restr("saltamontes | orangutan");
	
	conjS tags1, tags2, tags3;
	tags1.agregar("ballena");
	
	tags2.agregar("orangutan");
	tags2.agregar("ballena");
	
	tags3.agregar("serpiente");
	
	m->Agregar(a);
	m->Agregar(b);
	m->Agregar(c);
	m->Agregar(d);
	m->Agregar(e);
	
	m->Conectar(a, b, r1);
	//cout << (m->Conectadas(a, b) ? "Conectadas" : "No conectadas") << endl;
	//cout << (m->Conectadas(b, a) ? "Conectadas" : "No conectadas") << endl;
	m->Conectar(b, a, r2);
	//cout << (m->Conectadas(b, a) ? "Conectadas" : "No conectadas") << endl;
	m->Conectar(b, c, r5);
	//cout << (m->Conectadas(b, c) ? "Conectadas" : "No conectadas") << endl;
	*/
	/*
	cout << m->Restriccion(b, a).verifica(tags2) << endl;
	cout << m->Restriccion(a, b).verifica(tags3) << endl;
	cout << m->Restriccion(a, b).verifica(tags2) << endl;
	
	Conj<Estacion>::const_Iterador itEst = m->Estaciones().CrearIt();
	cout << "Estaciones: \n";
	while(itEst.HaySiguiente()){
		cout << itEst.Siguiente() << endl;
		itEst.Avanzar();
	}
	cout << "\n";
	
	Conj<Mapa::senda>::const_Iterador itSendas = m->Sendas();
	while(itSendas.HaySiguiente()){
		cout << "e1: " << itSendas.Siguiente().e1 << " e2: " << itSendas.Siguiente().e2 << endl;
		itSendas.Avanzar();
	}
	*/
	/*
	conjS stuff, stuff2, stuff3;
	
	stuff.agregar("orangutan");
	
	stuff2.agregar("ballena");
	stuff2.agregar("serpiente");
	
	stuff3.agregar("ballena");
	
	Ciudad *ciudad = new Ciudad(*m);
	
	cout << ciudad->mapa().Estaciones() << std::endl;
	cout << ciudad->proximaRUR() << std::endl;
	cout << std::endl;
	ciudad->entrar(stuff, "Paris");
	cout << ciudad->proximaRUR() << std::endl;
	cout << ciudad->estacion(0) << std::endl;
	cout << ciudad->infracciones(0) << std::endl;
	cout << ciudad->tags(0).pertenece("orangutan") << std::endl;
	
	cout << std::endl;
	ciudad->entrar(stuff2, "Nevada");
	cout << ciudad->proximaRUR() << std::endl;
	cout << ciudad->estacion(1) << std::endl;
	cout << ciudad->infracciones(1) << std::endl;
	cout << ciudad->tags(1).pertenece("serpiente") << std::endl;
	
	cout << std::endl;
	ciudad->entrar(stuff3, "Texas");
	cout << ciudad->proximaRUR() << std::endl;
	cout << ciudad->estacion(2) << std::endl;
	cout << ciudad->infracciones(2) << std::endl;
	cout << ciudad->tags(2).pertenece("ballena") << std::endl;
	cout << "Texas, Paris " << ciudad->mapa().Conectadas("Texas", "Paris") << std::endl;
	
	cout << std::endl;
	ciudad->mover(2, "Paris");
	cout << ciudad->proximaRUR() << std::endl;
	cout << ciudad->estacion(2) << std::endl;
	cout << ciudad->infracciones(2) << std::endl;
	cout << ciudad->tags(2).pertenece("ballena") << std::endl;
	
	Conj<RUR>::const_Iterador itR = ciudad->robots();
	cout << "Robots: ";
	while(itR.HaySiguiente()){
		cout << itR.Siguiente();
		itR.Avanzar();
		if (itR.HaySiguiente())
			cout << ", ";
		else
			cout << endl;
	}
	
	ciudad->inspeccion("Paris");
	itR = ciudad->robots();
	cout << "\nLuego de inspección...\n";
	cout << "Robots: ";
	while(itR.HaySiguiente()){
		cout << itR.Siguiente();
		itR.Avanzar();
		if (itR.HaySiguiente())
			cout << ", ";
		else
			cout << endl;
	}
	cout << endl;
	
	delete ciudad;
	delete m;
	*/
	/*
	
	std::cout << ((stuff.pertenece("Roberto")) ? "Sí" : "No") << std::endl;
	std::cout << ((stuff.pertenece("Robert")) ? "Sí" : "No") << std::endl;
	std::cout << ((stuff.pertenece("Rob")) ? "Sí" : "No") << std::endl;
	std::cout << ((stuff.pertenece("Chimi")) ? "Sí" : "No") << std::endl;
	std::cout << ((stuff.pertenece("Changa")) ? "Sí" : "No") << std::endl;
	std::cout << ((stuff.pertenece("Stupidity")) ? "Sí" : "No") << std::endl;
	*/
	/*
		cdp<int> hola;
	cdp<int>::itCola it;
	
	hola.encolar(8);
	it = hola.encolar(1);
	hola.encolar(2);
	hola.encolar(4);
	hola.encolar(7);
	hola.encolar(5);
	hola.encolar(3);
	hola.encolar(6);

	while(!hola.vacia()){
		std::cout << "proximo: " << it.actual() << "\n";
		it.itEliminar();
	}

	while(!hola.vacia()){
		std::cout << "proximo: " << hola.proximo() << "\n";
		hola.desencolar();
	}
	
	
	*/
	
	//aed2::ArbolSintactico *ab = aed2::ArbolSintactico::LeerDeString("carne | !pollo & arroz");
	//std::cout << ab->raiz << std::endl;
	//std::cout << ab->aString() << std::endl;
	//delete ab;
	
	/*
	DiccS<int> diccionario;
	int a = 2;
	int b = 3;
	int c = 4;
	int d = 5;
	int e = 6;
	diccionario.definir("Pedro", a);
	diccionario.definir("Pedra", b);
	diccionario.definir("Jorge", c);
	diccionario.definir("Jorgelin", d);
	diccionario.definir("adasdadas", e);
	
	std::cout << diccionario.claves() << std::endl;
	std::cout << diccionario.obtener("Pedro") << std::endl;
	std::cout << diccionario.obtener("Pedra") << std::endl;
	std::cout << diccionario.obtener("Jorge") << std::endl;
	std::cout << diccionario.obtener("Jorgelin") << std::endl;
	std::cout << diccionario.obtener("adasdadas") << std::endl;
	*/
	
	return 0;
}
