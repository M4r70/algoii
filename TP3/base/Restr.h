/*
 * Author: MathiaZ
 *
 * Restriccion.
 * 
 */

#ifndef restrr
#define	restrr

#include <iostream>
#include "ArbolSintactico.h"
#include "aed2.h"
#include "conjstr.h"

using namespace aed2;

class Restr{

public:
	Restr(const String&);
	Restr(const Restr&);
	bool verifica(const conjS&) const;
	~Restr();
	String verRestr() const;

private:
	ArbolSintactico *arbol;
	bool verificaAux(ArbolSintactico*, const conjS&) const; 
};

#endif	//restrr
