/*
 * Author: 
 *
 * Mapa (DiccS(DiccS(restr)))
 * 
 */

#ifndef MapaAED2
#define MapaAED2

#include <ostream>
#include "diccs.h"
#include "Restr.h"
#include "aed2.h"

using namespace aed2;

class Mapa
{
  public:
  
  struct senda {
  	senda(const Estacion& es1, const Estacion& es2, const Restr& re) : e1(es1), e2(es2), r(re){
  		s = re.verRestr();
  	};
  	bool operator==(const senda& otra) const;
  	bool operator!=(const senda& otra) const;
  	const Estacion e1;
 	const Estacion e2; 
 	const Restr r;
 	String s; //Agregado, porque el Driver es malo
  };
  
  //Crea un mapa vacio
  Mapa();
  
  ~Mapa();
  
  //Agrega una Estacion al mapa (La define en el diccS)
  //PRE: e no esta en el mapa
  void Agregar(const Estacion& e);
  
  //Conectar 2 Estaciones (Las define mutuamente en los diccS de e1 y e2)
  //PRE: e1 y e2 estan en el mapa
  void Conectar(const Estacion& e1, const Estacion& e2, Restr& r);
  
  //Devuelve el conjunto de Estaciones en el mapa
  const Conj<Estacion>& Estaciones() const;
  
  //Devuelve si estan conectadas 2 Estaciones
  //PRE: e1 y e2 estan en el mapa
  bool Conectadas(const Estacion& e1, const Estacion& e2) const;
  
  //Devuelve la Restr de la senda 
  //PRE: e1 y e2 estan conectados
  Restr& Restriccion(const Estacion& e1, const Estacion& e2) const;
  
  //Devuelve el conjunto de sendas en el mapa
  Conj<senda>::const_Iterador Sendas();
  
  private:
  	DiccS<DiccS<Restr> > mapa;
  	Lista<const DiccS<Restr>* > _iter; // Agregado para eliminar los DiccS que se van creando
  	Conj<senda> *sendas; 	// Agregado para que no se invaliden las sendas al devolverlas
};


#endif
